# Try out Knock

## 1 - Create new app

```
$ mkdir try_out_knock && cd $_
$ rvm use ruby-2.5.1@rails_guides_blog --ruby-version --create
$ gem install rails
$ rails new .
```

## 2 - Add User

```
$ rails g scaffold User name:string email:string password_digest:string admin:boolean
```

*app/models/user.rb*
```
class User < ApplicationRecord
  has_secure_password

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  validates_presence_of :name, :password_digest

  validates :email,
            presence: true,
            length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }
end
```

*Gemfile*
```
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
```
```
$ bundle install
```

*XXX_create_users.rb*
```
      t.boolean :admin, default: false
```

```
$ rails db:migrate
```

## 3 - Seed User Database

*db/seeds.rb*

```
admin = User.new
admin.name = 'Harry Admin'
admin.email = 'admin@harry.com'
admin.password = 'harryAdmin'
admin.password_confirmation = 'harryAdmin'
admin.admin = true
admin.save

user = User.new
user.name = 'Harry Bro'
user.email = 'user@harry.com'
user.password = 'harryBro'
user.password_confirmation = 'harryBro'
user.save
```

```
$ rails db:seed
```

To check, run
```
$ rcs
> User.all
```

## 4 - Knock

```
gem 'knock'
```
```
$ bundle install
```

```
$ rails generate knock:install
```

*config/initializers/knock.rb*
```
Knock.setup do |config|
  config.token_lifetime = nil
  [...]
  config.token_secret_signature_key = -> { Rails.application.credentials.read }
end
```

```
$ rails generate knock:token_controller user
```

*app/controllers/user_token_controller.rb*
```
class UserTokenController < Knock::AuthTokenController
  skip_before_action :verify_authenticity_token
end
```

*app/controllers/application_controller.rb*
```
class ApplicationController < ActionController::API
  include Knock::Authenticable
  
  before_action :authenticate_user
end
```

*routes.rb*
```
Rails.application.routes.draw do
  post 'user_token' => 'user_token#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
```

## 5 - Todos

```
$ rails g scaffold Todo name:string description:text user:references
```

*app/models/user.rb*
```
class User < ApplicationRecord
  has_many :todos
  [...]
end
```